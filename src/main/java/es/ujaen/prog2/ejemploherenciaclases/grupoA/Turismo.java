/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejemploherenciaclases.grupoA;

/**
 *
 * @author UJA
 */
public class Turismo extends Vehiculo {

    private int nPuertas;

    public Turismo(int nPuertas, String matricula, int capacidadPasajeros, TipoCombustible combustible, String marcaModelo) {
        super(matricula, capacidadPasajeros, combustible, marcaModelo);
        this.nPuertas = nPuertas;
    }

    public int getnPuertas() {
        return nPuertas;
    }

    public void setnPuertas(int nPuertas) {
        this.nPuertas = nPuertas;
    }

    @Override
    public String to_string() {
        String resultado = "Turismo " + getMarcaModelo() + " con matrícula " + getMatricula()
                + ",  " + nPuertas + " puertas, " + getCapacidadPasajeros()
                + " pasajeros y combustible " + getCombustible().toString();
        return resultado;
    }

}
