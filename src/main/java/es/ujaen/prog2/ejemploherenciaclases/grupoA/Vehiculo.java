/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejemploherenciaclases.grupoA;

/**
 *
 * @author UJA
 */
public abstract class Vehiculo {

    public enum TipoCombustible {
        Gasolina, Diesel, GLP,
        GNC, Eléctrico, Híbrido;

        @Override
        public String toString() {
            switch (this) {
                case Gasolina:
                    return "Gasolina";
                case Diesel:
                    return "Diesel";
                case GLP:
                    return "GLP";
                case GNC:
                    return "GNC";
                case Eléctrico:
                    return "Eléctrico";
                case Híbrido:
                    return "Híbrido";
            }
            return "Error";
        }
    };

    private String matricula;
    private int capacidadPasajeros;
    private TipoCombustible combustible;
    private String marcaModelo;

    public Vehiculo(String matricula, int capacidadPasajeros, TipoCombustible combustible, String marcaModelo) {
        this.matricula = matricula;
        this.capacidadPasajeros = capacidadPasajeros;
        this.combustible = combustible;
        this.marcaModelo = marcaModelo;
    }
           
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getCapacidadPasajeros() {
        return capacidadPasajeros;
    }

    public void setCapacidadPasajeros(int capacidadPasajeros) {
        this.capacidadPasajeros = capacidadPasajeros;
    }

    public TipoCombustible getCombustible() {
        return combustible;
    }

    public void setCombustible(TipoCombustible combustible) {
        this.combustible = combustible;
    }

    public String getMarcaModelo() {
        return marcaModelo;
    }

    public void setMarcaModelo(String marcaModelo) {
        this.marcaModelo = marcaModelo;
    }

    public abstract String to_string();

}
