/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejemploherenciaclases.grupoB;

/**
 *
 * @author UJA
 */
public class Camion extends Vehiculo {

    float capacidadPeso;
    float capacidadVolumen;

    public Camion(float capacidadPeso, float capacidadVolumen, String matricula, int capacidadPasajeros, TipoCombustible combustible, String marcaModelo) {
        super(matricula, capacidadPasajeros, combustible, marcaModelo);
        this.capacidadPeso = capacidadPeso;
        this.capacidadVolumen = capacidadVolumen;
    }

    public float getCapacidadPeso() {
        return capacidadPeso;
    }

    public void setCapacidadPeso(float capacidadPeso) {
        this.capacidadPeso = capacidadPeso;
    }

    public float getCapacidadVolumen() {
        return capacidadVolumen;
    }

    public void setCapacidadVolumen(float capacidadVolumen) {
        this.capacidadVolumen = capacidadVolumen;
    }

    
    
    @Override
    public String to_string() {
        String resultado = "Camion " + getMarcaModelo() + " con matrícula " + getMatricula()
                + ",  " + capacidadVolumen + " m3 y " + capacidadPeso + " kilos de capacidad, "
                + getCapacidadPasajeros() + " pasajeros y combustible " + getCombustible().toString();
        return resultado;
    }

}
