/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejemploherenciaclases.grupoB;

import java.util.ArrayList;

/**
 *
 * @author UJA
 */
public class MainEjemploGrupoB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Turismo tur1 = new Turismo(5, "J 0435W", 4, Vehiculo.TipoCombustible.Diesel, "Renault 204");
        Turismo tur2 = new Turismo(4, "7890 SOT", 5, Vehiculo.TipoCombustible.Gasolina, "Ford Fiesta");
        Turismo tur3 = new Turismo(4, "4567 WRP", 4, Vehiculo.TipoCombustible.GLP, "Opel Astra");

        Camion cam1 = new Camion(2500, 18, "1234 RTY", 2, Vehiculo.TipoCombustible.GNC, "CAT SX890");
        Camion cam2 = new Camion(500, 8, "GR 5678F", 2, Vehiculo.TipoCombustible.GNC, "Renault Express");
        Camion cam3 = new Camion(1500, 12, "3456 HJK", 3, Vehiculo.TipoCombustible.Eléctrico, "FORD UL15D");
        
        ArrayList<Vehiculo> vehiculos = new ArrayList<>();
        
        vehiculos.add(tur1);
        vehiculos.add(tur2);
        vehiculos.add(tur3);
        vehiculos.add(cam1);
        vehiculos.add(cam2);
        vehiculos.add(cam3);
          
        for (int i = 0; i < vehiculos.size(); i++) {
            Vehiculo vAux = vehiculos.get(i);
            if(vAux instanceof Turismo && ((Turismo)vAux).getnPuertas() >= 5)
                vAux.setCapacidadPasajeros(vAux.getCapacidadPasajeros() + 1);
        }
        
        System.out.println("Camiones:");
        for (int i = 0; i < vehiculos.size(); i++) {
            if(vehiculos.get(i) instanceof Camion)
            System.out.println(vehiculos.get(i).to_string());
        }

        
        System.out.println("");
        System.out.println("Turismos:");
        for (int i = 0; i < vehiculos.size(); i++) {
            if(vehiculos.get(i) instanceof Turismo)
            System.out.println(vehiculos.get(i).to_string());
        }

        
    }

}
